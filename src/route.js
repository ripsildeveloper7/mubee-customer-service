var accountRoute = require('./account/accountRoute');
var settingRoute = require('./settings/settingRoute');
var customerMarketingRoute = require('./customerMarketing/customerMarketingRoute');

exports.loadRoutes = function (app) {
  accountRoute(app);
  settingRoute(app);
  customerMarketingRoute(app);
};


var customerDetails = require('../../model/customerAccount.model');
var masterAccount = require('../../model/masterAccount.model');
var erpAccount = require('../../model/erpAccount.model');
var adminAccountDetails = require('../../model/adminAccount.model');
var VendorDetail = require('../../model/vendorAccount.model');
const AWS = require('aws-sdk');
var env = require('../../config/s3.env');
var nodemailer = require('nodemailer');

exports.createCustomer = function (req, res) {
    customerDetails.find({
        'emailId': req.body.emailId
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length !== 0) {
                res.status(200).send({
                    "result": "Already exist"
                })
            } else {
                var currentDate = new Date();
                var date = currentDate.getDate();
                var month = currentDate.getMonth() + 1;
                var year = currentDate.getFullYear();
                var todayDate = month + '/' + date + '/' + year;
                var create = new customerDetails();
                create.emailId = req.body.emailId;
                create.mobileNumber = req.body.mobileNumber;
                create.password = req.body.password;
                create.date = todayDate;
                create.publish = true;
                create.save(function (err, customerData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        var createMaster = new masterAccount();
                        createMaster.emailId = req.body.emailId;
                        createMaster.mobileNumber = req.body.mobileNumber;
                        createMaster.password = req.body.password;
                        createMaster.userId = customerData._id;
                        createMaster.save(function (err, masterData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(masterData);
                            }
                        })
                    }
                })
            }
        }
    })
}


exports.createCustomerForLandingPage = function (req, res) {
    customerDetails.find({
        'emailId': req.body.emailId
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length !== 0) {
                res.status(200).send({
                    "result": "Already exist"
                })
            } else {
                var currentDate = new Date();
                var date = currentDate.getDate();
                var month = currentDate.getMonth() + 1;
                var year = currentDate.getFullYear();
                var todayDate = month + '/' + date + '/' + year;
                var create = new customerDetails();
                create.emailId = req.body.emailId;
                create.mobileNumber = req.body.mobileNumber;
                create.password = req.body.password;
                create.date = todayDate;
                create.save(function (err, customerData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        var createMaster = new masterAccount();
                        createMaster.emailId = req.body.emailId;
                        createMaster.mobileNumber = req.body.mobileNumber;
                        createMaster.password = req.body.password;
                        createMaster.userId = customerData._id;
                        createMaster.save(function (err, masterData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                // var code = 'UCLP1001'
                                // let transporter = nodemailer.createTransport({
                                //     service: 'Godaddy',
                                //     host: "smtpout.secureserver.net",
                                //     secureConnection: true,
                                //     port: 465,
                                //     auth: {
                                //         user: 'support@ucchalfashion.com',
                                //         pass: 'Enterprise@2019'
                                //     },
                                //     debug: true,
                                // });
                                // var mailOptions = {
                                //     from: 'support@ucchalfashion.com',
                                //     to: req.body.emailId,
                                //     subject: 'PROMO CODE',
                                //     html: '<html><body> <p> Congratulations, <br>  Your promo code is : UCLP1001. Kindly use the promo code on the day of launch to get exciting discounts as launch offer. We are eagerly looking forward for your visit to our website www.ucchalfashion.com. ' + code + '</p></body></html>'
                                // };
                                // transporter.sendMail(mailOptions, function (error, info) {
                                //     if (error) {
                                //         console.log(error);
                                //     } else {
                                //         customerDetails.findOne({
                                //             '_id': customerData._id
                                //         }).select().exec(function (err, customer) {
                                //             if (err) {
                                //                 res.status(500).json(err);
                                //             } else {
                                //                 customer.subscribedCustomer = true;
                                //                 customer.code = code;
                                //                 customer.save(function (err, codeData) {
                                //                     if (err) {
                                //                         console.log(err);
                                //                     } else {
                                //                         res.status(200).json(codeData)
                                //                     }
                                //                 })
                                //             }
                                //         })

                                //     }
                                // });
                                res.status(200).json(masterData)
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.getProfileCustomer = function (req, res) {
    customerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, profileData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(profileData);
        }
    })
}

exports.uploadAddress = function (req, res) {
    customerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(profileData);
        }
    })
}

exports.updateAddressDetails = function (req, res) {
    let addressData = {
        fname: req.body.fname,
        lname: req.body.lname,
        mobileNumber: req.body.mobileNumber,
        streetAddress: req.body.streetAddress,
        building: req.body.building,
        landmark: req.body.landmark,
        city: req.body.city,
        state: req.body.state,
        pincode: req.body.pincode,
        country: req.body.country
    };
    customerDetails.findOneAndUpdate({
        '_id': req.params.id
    }, {
        $push: {
            addressDetails: addressData
        }
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            customerDetails.find({
                '_id': req.params.id
            }).select().exec(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.updateCardsDetails = function (req, res) {
    let cardData = {
        cardName: req.body.cardName,
        cardNumber: req.body.cardNumber,
        expiryMonth: req.body.expiryMonth,
        expiryYear: req.body.expiryYear
    }
    customerDetails.findOneAndUpdate({
        '_id': req.params.id
    }, {
        $push: {
            cardDetails: cardData
        }
    }, function (err, updateData) {
        if (err) {
            res.status(500).json(err);
        } else {
            customerDetails.find({}).select().exec(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.updateProfileUpdate = function (req, res) {
    customerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.emailId = req.body.emailId;
            findData.mobileNumber = req.body.mobileNumber;
            findData.password = req.body.password;
            findData.lastName = req.body.lastName;
            findData.firstName = req.body.firstName;
            findData.dateOfBirth = req.body.dateOfBirth;
            findData.location = req.body.location;
            findData.gender = req.body.gender;
            findData.save(function (err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    masterAccount.findOne({
                        'userId': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            data.emailId = req.body.emailId;
                            data.mobileNumber = req.body.mobileNumber;
                            data.password = req.body.password;
                            data.save(function (err, data) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    customerDetails.findOne({
                                        '_id': req.params.id
                                    }).select().exec(function (err, findOneData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findOneData)
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

exports.customerAddressUpdate = function (req, res) {
    customerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.addressDetails.id(req.params.addressId);
            addressDetail.fname = req.body.fname;
            addressDetail.lname = req.body.lname;
            addressDetail.mobileNumber = req.body.mobileNumber;
            addressDetail.streetAddress = req.body.streetAddress;
            addressDetail.building = req.body.building;
            addressDetail.landmark = req.body.landmark;
            addressDetail.city = req.body.city;
            addressDetail.state = req.body.state;
            addressDetail.pincode = req.body.pincode;
            addressDetail.country = req.body.country;
            findData.save(function (err, addressData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    customerDetails.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.customerCardDetailUpdate = function (req, res) {
    customerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var cardData = findData.cardDetails.id(req.params.cardId);
            cardData.cardName = req.body.cardName;
            cardData.cardNumber = req.body.cardNumber;
            cardData.expiryMonth = req.body.expiryMonth;
            cardData.expiryYear = req.body.expiryYear;
            findData.save(function (err, Data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    customerDetails.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.deleteSeletedAddress = function (req, res) {
    customerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.addressDetails.id(req.params.addressId).remove();
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    customerDetails.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.deleteSeletedCard = function (req, res) {
    customerDetails.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.cardDetails.id(req.params.cardId).remove();
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    customerDetails.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.getAllCustomer = function (req, res) {
    customerDetails.find({}).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.deleteCustomer = function (req, res) {
    customerDetails.findByIdAndRemove({
        '_id': req.params.id
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);

        } else {
            masterAccount.findOneAndRemove({
                'userId': req.params.id
            }).select().exec(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    customerDetails.find({}).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.customerOverallCount = function (req, res) {
    customerDetails.find({}).count().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.recentRegisteredCustomer = function (req, res) {
    customerDetails.find({}).sort({
        'date': -1
    }).limit(5).exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.customerTodayCount = function (req, res) {
    var currentDate = new Date();
    var date = currentDate.getDate();
    var month = currentDate.getMonth() + 1;
    var year = currentDate.getFullYear();
    var dateString = month + '/' + date + '/' + year;

    console.log(dateString);
    customerDetails.find({
        'date': dateString
    }).count().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.createCustomerByAdmin = function (req, res) {
    customerDetails.find({
        'emailId': req.body.emailId
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length !== 0) {
                res.status(200).send({
                    "result": "Already exist"
                })
            } else {
                var currentDate = new Date();
                var date = currentDate.getDate();
                var month = currentDate.getMonth() + 1;
                var year = currentDate.getFullYear();
                var todayDate = month + '/' + date + '/' + year;
                var create = new customerDetails();
                create.emailId = req.body.emailId;
                create.mobileNumber = req.body.mobileNumber;
                create.password = req.body.password;
                create.date = todayDate;
                create.firstName = req.body.firstName;
                create.lastName = req.body.lastName;
                create.gender = req.body.gender;
                create.location = req.body.location;
                create.dateOfBirth = req.body.dateOfBirth;
                create.save(function (err, customerData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        var createMaster = new masterAccount();
                        createMaster.emailId = req.body.emailId;
                        createMaster.mobileNumber = req.body.mobileNumber;
                        createMaster.password = req.body.password;
                        createMaster.userId = customerData._id;
                        createMaster.save(function (err, masterData) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(customerData);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.deleteCustomerAccount = function (req, res) {
    customerDetails.findByIdAndRemove(req.params.id, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(data);
        }
    })
}

exports.createErpVendor = function (req, res) {
    var create = new erpAccount();
    create.emailId = req.body.emailId;
    create.password = req.body.password;
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}
exports.createAdminAccount = function (req, res) {
    var create = new adminAccountDetails();
    create.emailId = req.body.emailId;
    create.password = req.body.password;
    create.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}


// Vendor Form ----------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------

exports.createVendor = function (req, res, dateString, vendorReg) {
    VendorDetail.find({
        'vendorEmailId': req.body.vendorEmailId
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new VendorDetail();
                var passwordValue = Math.random().toString(36).substring(10) + (new Date()).getTime().toString(36);
                create.vendorName = req.body.vendorName;
                create.vendorEmailId = req.body.vendorEmailId;
                create.vendorMobileNumber = req.body.vendorMobileNumber;
                create.password = passwordValue;
                create.vendorType = req.body.vendorType;
                create.vendorContactPerson = req.body.vendorContactPerson;
                create.vendorContactPersonEmailId = req.body.vendorContactPersonEmailId;
                create.contractReviewDate = req.body.contractReviewDate;
                create.vendorContactPersonMobileNumber = req.body.vendorContactPersonMobileNumber;
                create.vendorContactPersonDesignation = req.body.vendorContactPersonDesignation;
                create.officeAddressDetails = req.body.officeAddressDetails;
                create.supplyLocationAddressDetails = req.body.supplyLocationAddressDetails;
                create.merchandiseDivision = req.body.merchandiseDivision;
                create.companyStatus = req.body.companyStatus;
                create.contractNumber = req.body.contractNumber;
                create.contractDate = req.body.contractDate;
                create.contractStartDate = req.body.contractStartDate;
                create.contractEndDate = req.body.contractEndDate;
                create.paymentDetails = req.body.paymentDetails;
                create.deliveryDetails = req.body.deliveryDetails;
                create.performanceDetails = req.body.performanceDetails;
                create.signatureDetails = req.body.signatureDetails;
                create.vendorCode = vendorReg;
                create.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                res.status(200).send({
                    "result": "E-mail already exist"
                })
            }
        }
    })
}


exports.getAllVendor = function (req, res) {
    VendorDetail.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.deleteVendor = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var s3 = new AWS.S3();
            s3.deleteObject({
                Bucket: env.Bucket,
                Key: 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'digitalsignature' + '/' + findData.digitalSignature
            }, function (err, deleteDigital) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    s3.deleteObject({
                        Bucket: env.Bucket,
                        Key: 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'cancelcheque' + '/' + findData.cancelledCheque
                    }, function (err, deleteCheque) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            VendorDetail.findByIdAndRemove({
                                '_id': req.params.id
                            }, function (err, data) {
                                if (err) {
                                    res.status(500).json(err);

                                } else {
                                    VendorDetail.find({}).select().exec(function (err, findData) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            res.status(200).json(findData);
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })

}

exports.vendorAddressUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.addressDetails.id(req.params.addressId);
            addressDetail.addressProofName = req.body.addressProofName;
            addressDetail.addressLine1 = req.body.addressLine1;
            addressDetail.addressLine2 = req.body.addressLine2;
            addressDetail.city = req.body.city;
            addressDetail.state = req.body.state;
            addressDetail.pincode = req.body.pincode;
            findData.save(function (err, addressData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.deleteSeletedVendorAddress = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.addressDetails.id(req.params.addressId).remove();
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.uploadVendorAddressDetails = function (req, res) {
    let addressData = {
        addressProofName: req.body.addressProofName,
        addressLine1: req.body.addressLine1,
        addressLine2: req.body.addressLine2,
        city: req.body.city,
        state: req.body.state,
        pincode: req.body.pincode
    };
    VendorDetail.findOneAndUpdate({
        '_id': req.params.id
    }, {
        $push: {
            addressDetails: addressData
        }
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            VendorDetail.find({
                '_id': req.params.id
            }).select().exec(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.getVendorProfile = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, profileData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(profileData);
        }
    })
}

/* office Address */

exports.vendorOfficeAddressUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.officeAddressDetails.id(req.params.addressId);
            addressDetail.officeAddressLine1 = req.body.officeAddressLine1;
            addressDetail.officeAddressLine2 = req.body.officeAddressLine2;
            addressDetail.officeCity = req.body.officeCity;
            addressDetail.officeState = req.body.officeState;
            addressDetail.officePincode = req.body.officePincode;
            findData.save(function (err, addressData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.vendorSupplyAddressUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.supplyLocationAddressDetails.id(req.params.addressId);
            addressDetail.supplyAddressLine1 = req.body.supplyAddressLine1;
            addressDetail.supplyAddressLine2 = req.body.supplyAddressLine2;
            addressDetail.supplyCity = req.body.supplyCity;
            addressDetail.supplyState = req.body.supplyState;
            addressDetail.supplyPincode = req.body.supplyPincode;
            findData.save(function (err, addressData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}
exports.vendorPaymentUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.paymentDetails.id(req.params.addressId);
            addressDetail.currency = req.body.currency,
                addressDetail.paymentDay = req.body.paymentDay,
                addressDetail.creditLimit = req.body.creditLimit,
                addressDetail.taxesType = req.body.taxesType,
                addressDetail.otherPaymentTerm = req.body.otherPaymentTerm,
                addressDetail.formsRequired = req.body.formsRequired,
                addressDetail.modeOfPayment = req.body.modeOfPayment,
                addressDetail.panNo = req.body.panNo,
                addressDetail.tinNo = req.body.tinNo,
                addressDetail.cstNo = req.body.cstNo,
                addressDetail.gstNo = req.body.gstNo,
                addressDetail.bankName = req.body.bankName,
                addressDetail.bankBranch = req.body.bankBranch,
                addressDetail.bankAddress = req.body.bankAddress,
                addressDetail.bankAccountNumber = req.body.bankAccountNumber,
                addressDetail.micrCode = req.body.micrCode,
                addressDetail.neftCode = req.body.neftCode,
                addressDetail.rtgsCode = req.body.rtgsCode,
                addressDetail.tanNo = req.body.tanNo,
                findData.save(function (err, addressData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        VendorDetail.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
        }
    })
}

exports.vendorDeliveryUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.deliveryDetails.id(req.params.addressId);
            addressDetail.slsOT = req.body.slsOT,
                addressDetail.slsIFC = req.body.slsIFC,
                addressDetail.slsIFL = req.body.slsIFL,
                addressDetail.deliveryMode = req.body.deliveryMode,
                addressDetail.transportCostPaidBy = req.body.transportCostPaidBy,
                addressDetail.returnOfGoods = req.body.returnOfGoods,
                addressDetail.minOrderSizeQty = req.body.minOrderSizeQty,
                addressDetail.minOrderSizeAmt = req.body.minOrderSizeAmt,
                addressDetail.cateImgAssignBy = req.body.cateImgAssignBy,
                addressDetail.chargesOfPhotoShopBy = req.body.chargesOfPhotoShopBy,
                addressDetail.photosCharge = req.body.photosCharge,
                addressDetail.businessModel = req.body.businessModel,
                addressDetail.defaultDeliveyLocation = req.body.defaultDeliveyLocation,
                findData.save(function (err, addressData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        VendorDetail.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
        }
    })
}

exports.vendorPerformanceUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.performanceDetails.id(req.params.addressId);
            addressDetail.target = req.body.target,
                addressDetail.unconditionalDiscount = req.body.unconditionalDiscount,
                addressDetail.tradingDiscount = req.body.tradingDiscount,
                addressDetail.orderSize = req.body.orderSize,
                addressDetail.cashDiscount = req.body.cashDiscount,
                addressDetail.cashDiscountPaid = req.body.cashDiscountPaid,
                addressDetail.promotionalProgram = req.body.promotionalProgram,
                addressDetail.catalogueParticipation = req.body.catalogueParticipation,
                addressDetail.newLaunches = req.body.newLaunches,
                addressDetail.themeParticipation = req.body.themeParticipation,
                addressDetail.specilalization = req.body.specilalization,
                addressDetail.liquidationOffer = req.body.liquidationOffer,
                findData.save(function (err, addressData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        VendorDetail.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
        }
    })
}

exports.vendorSignatureUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.signatureDetails.id(req.params.addressId);
            addressDetail.vendorAuth = req.body.vendorAuth,
                addressDetail.vendDate = req.body.vendDate,
                addressDetail.companyAuth = req.body.companyAuth,
                addressDetail.companyDate = req.body.companyDate,
                addressDetail.regBy = req.body.regBy,
                addressDetail.regDate = req.body.regDate,
                findData.save(function (err, addressData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        VendorDetail.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
        }
    })
}
exports.deleteSeletedVendorOfficeAddress = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.officeAddressDetails.id(req.params.addressId).remove();
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.uploadVendorOfficeAddressDetails = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.officeAddressDetails.id(req.params.addressId);
            addressDetail.wareHouseAddressProofName = req.body.wareHouseAddressProofName;
            addressDetail.addressLine1 = req.body.addressLine1;
            addressDetail.addressLine2 = req.body.addressLine2;
            addressDetail.city = req.body.city;
            addressDetail.state = req.body.state;
            addressDetail.pincode = req.body.pincode;
            findData.save(function (err, addressData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

// warehouse address


exports.vendorWarehouseAddressUpdate = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var addressDetail = findData.wareHouseAddressDetails.id(req.params.addressId);
            addressDetail.wareHouseAddressProofName = req.body.wareHouseAddressProofName;
            addressDetail.addressLine1 = req.body.addressLine1;
            addressDetail.addressLine2 = req.body.addressLine2;
            addressDetail.city = req.body.city;
            addressDetail.state = req.body.state;
            addressDetail.pincode = req.body.pincode;
            findData.save(function (err, addressData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.deleteSeletedVendorWarehouseAddress = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.wareHouseAddressDetails.id(req.params.addressId).remove();
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    VendorDetail.findOne({
                        '_id': req.params.id
                    }).select().exec(function (err, data) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(data);
                        }
                    })
                }
            })
        }
    })
}

exports.uploadVendorWarehouseAddressDetails = function (req, res) {
    let addressData = {
        wareHouseAddressProofName: req.body.wareHouseAddressProofName,
        addressLine1: req.body.addressLine1,
        addressLine2: req.body.addressLine2,
        city: req.body.city,
        state: req.body.state,
        pincode: req.body.pincode
    };
    VendorDetail.findOneAndUpdate({
        '_id': req.params.id
    }, {
        $push: {
            wareHouseAddressDetails: addressData
        }
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            VendorDetail.find({
                '_id': req.params.id
            }).select().exec(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.updateVendorBasicDetails = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {

            findData.vendorName = req.body.vendorName;
            findData.vendorEmailId = req.body.vendorEmailId;
            findData.vendorMobileNumber = req.body.vendorMobileNumber;
            findData.vendorType = req.body.vendorType;
            findData.vendorContactPerson = req.body.vendorContactPerson;
            findData.vendorContactPersonMobileNumber = req.body.vendorContactPersonMobileNumber;
            findData.vendorContactPersonEmailId = req.body.vendorContactPersonEmailId;
            findData.vendorContactPersonDesignation = req.body.vendorContactPersonDesignation;
            findData.merchandiseDivision = req.body.merchandiseDivision;
            findData.companyStatus = req.body.companyStatus;
            findData.contractNumber = req.body.contractNumber;
            findData.contractStartDate = req.body.contractStartDate;
            findData.contractEndDate = req.body.contractEndDate;
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}
exports.updateVendorPaymentDetails = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.accountNumber = req.body.accountNumber;
            findData.ifscCode = req.body.ifscCode;
            findData.accountHolderName = req.body.accountHolderName;
            findData.accountType = req.body.accountType;
            findData.bankName = req.body.bankName;
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.updateVendorOtherDetails = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.tinNo = req.body.tinNo;
            findData.gstNo = req.body.gstNo;
            findData.panNo = req.body.panNo;
            findData.sellingProductType = req.body.sellingProductType;
            findData.customerType = req.body.customerType;
            findData.annualTurnover = req.body.annualTurnover;
            findData.noOfProduct = req.body.noOfProduct;
            findData.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}


exports.updateName = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            data.vendorName = req.body.vendorName;
            data.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}
exports.updateEmail = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            data.vendorEmailId = req.body.vendorEmailId;
            data.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}
exports.updateMobile = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            data.vendorMobileNumber = req.body.vendorMobileNumber;
            data.save(function (err, data) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(data);
                }
            })
        }
    })
}

exports.ChangesPassword = function (req, res) {
    VendorDetail.find({
        '_id': req.params.id,
        'password': req.body.oldPwd
    }).select().exec(function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data.length === 0) {
                res.status(200).send({
                    "result": "Password Not Currect"
                })
            } else {
                data[0].password = req.body.password;
                data[0].save(function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(data);
                    }
                })
            }
        }
    })
}

exports.createVendorByUpload = function (req, res, vendorReg) {
    VendorDetail.find({
        'vendorEmailId': req.body.vendorEmailId
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findData.length === 0) {
                var create = new VendorDetail();
                var passwordValue = Math.random().toString(36).substring(10) + (new Date()).getTime().toString(36);
                create.vendorName = req.body.vendorName;
                create.vendorEmailId = req.body.vendorEmailId;
                create.vendorMobileNumber = req.body.vendorMobileNumber;
                create.password = passwordValue;
                create.vendorType = req.body.vendorType;
                create.vendorContactPerson = req.body.vendorContactPerson;
                create.vendorContactPersonEmailId = req.body.vendorContactPersonEmailId;
                create.contractReviewDate = req.body.contractReviewDate;
                create.vendorContactPersonMobileNumber = req.body.vendorContactPersonMobileNumber;
                create.vendorContactPersonDesignation = req.body.vendorContactPersonDesignation;
                create.officeAddressDetails = req.body.officeAddressDetails;
                create.supplyLocationAddressDetails = req.body.supplyLocationAddressDetails;
                create.merchandiseDivision = req.body.merchandiseDivision;
                create.companyStatus = req.body.companyStatus;
                create.contractNumber = req.body.contractNumber;
                create.contractDate = req.body.contractDate;
                create.contractStartDate = req.body.contractStartDate;
                create.contractEndDate = req.body.contractEndDate;
                create.paymentDetails = req.body.paymentDetails;
                create.deliveryDetails = req.body.deliveryDetails;
                create.performanceDetails = req.body.performanceDetails;
                create.signatureDetails = req.body.signatureDetails;
                create.vendorCode = vendorReg;
                create.save(function (err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                res.status(200).send({
                    "result": "E-mail already exist"
                })
            }
        }
    })
}

exports.storeCancelChequeName = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.cancelledCheque = req.body.cancelledCheque;
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}

exports.storeDigitalSignatureName = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.digitalSignature = req.body.digitalSignature;
            findData.save(function (err, saveData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(saveData);
                }
            })
        }
    })
}

exports.updateImageCancelChequeName = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id,
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data.cancelledCheque === req.body.cancelledCheque) {
                res.status(200).json(data);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.Bucket,
                    Key: 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'cancelcheque' + '/' + data.cancelledCheque
                }, function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        VendorDetail.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data1) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                data1.cancelledCheque = req.body.cancelledCheque;
                                data1.save(function (err, val) {
                                    if (err) {
                                        res.status(500).json(err);
                                    } else {
                                        VendorDetail.find({}).select().exec(function (err, vendorData) {
                                            if (err) {
                                                res.status(500).json(err);
                                            } else {
                                                res.status(200).json(vendorData);
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }

        }
    });
}

exports.updateImageDigitalSignatureName = function (req, res) {
    VendorDetail.findOne({
        '_id': req.params.id,
    }, function (err, data) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (data.digitalSignature === req.body.digitalSignature) {
                res.status(200).json(data);
            } else {
                var s3 = new AWS.S3();
                s3.deleteObject({
                    Bucket: env.Bucket,
                    Key: 'images' + '/' + 'vendor' + '/' + req.params.id + '/' + 'digitalsignature' + '/' + data.digitalSignature
                }, function (err, data) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        VendorDetail.findOne({
                            '_id': req.params.id
                        }).select().exec(function (err, data1) {
                            if (err) {
                                res.status(500).json(err);
                            } else {
                                data1.digitalSignature = req.body.digitalSignature;
                                data1.save(function (err, val) {
                                    if (err) {
                                        res.status(500).json(err);
                                    } else {
                                        VendorDetail.find({}).select().exec(function (err, vendorData) {
                                            if (err) {
                                                res.status(500).json(err);
                                            } else {
                                                res.status(200).json(vendorData);
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }

        }
    });
}
exports.socialMediaLogin = function(req, res) {
    customerDetails.find({'emailId': req.body.emailId}).select().exec(function(err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            var currentDate = new Date();
            var date = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            var todayDate = month + '/' + date + '/' + year; 
            if (findData.length === 0) {
                var create = new customerDetails();
                create.emailId = req.body.emailId;
                create.createdBy = req.body.createdBy;
                create.firstName = req.body.firstName;
                create.lastName = req.body.lastName;
                create.date = todayDate;
                create.publish = true;
                create.lastLoginBy = req.body.createdBy;
                create.save(function(err, saveData) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        res.status(200).json(saveData);
                    }
                })
            } else {
                findData[0].lastLoginBy = req.body.createdBy;
                    findData[0].save(function(err, modifiedData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(modifiedData);
                        }
                    })
            }
        }
    })
}

exports.getCustomerAddedDate = function(req, res) {
    customerDetails.find().select('date').exec(function(err, vendor) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(vendor);
        }
    })
}
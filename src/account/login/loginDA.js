var masterAccount = require('../../model/masterAccount.model');
var erpAccount = require('../../model/erpAccount.model');
var adminAccount = require('../../model/adminAccount.model');
var vendorAccount = require('../../model/vendorAccount.model');

exports.login = function (req, res) {
    masterAccount.find({
        'emailId': req.body.emailId,
        'password': req.body.password
    }).select().exec(function (err, loginData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(loginData);
        }
    })
}

exports.erpLogin = function(req, res) {
    erpAccount.find({
        'emailId': req.body.emailId,
        'password': req.body.password
    }).select().exec(function(err,findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.adminAccount = function(req, res) {
    adminAccount.find({
        'emailId': req.body.emailId,
        'password': req.body.password
    }).select().exec(function(err,findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.loginVendor = function(req, res) {
    vendorAccount.find({
        'vendorEmailId': req.body.vendorEmailId,
        'password': req.body.password
    }).select().exec(function(err, loginData) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(loginData);
        }
    })
}
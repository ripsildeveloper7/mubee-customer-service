var registrationMgr = require('./registration/registrationMgr');
var loginMgr = require('./login/loginMgr');
var subscribeMgr = require('./subscribe/subscribeMgr');


module.exports = function (app) {

    //   Accounts Module

    app.route('/createcustomer') //  Customer Registration 
        .post(registrationMgr.createCustomer);

    app.route('/createcustomerforlandingpage') //  Customer Registration Landing Page
        .post(registrationMgr.createCustomerForLandingPage);    

    app.route('/createerpadmin') //  ERP Admin Registration 
        .post(registrationMgr.createErpVendor);    

    app.route('/createadminaccount') //  Admin Registration 
        .post(registrationMgr.createAdminAccount);       

    app.route('/customerlogin') //   Customer Login
        .post(loginMgr.login);

    app.route('/erplogin') //   Erp Admin Login
        .post(loginMgr.erpLogin);   
        
    app.route('/loginadminuser') //   Admin Login
        .post(loginMgr.adminAccount);       

    app.route('/getcustomerprofile/:id') // Get Customer Profile
        .get(registrationMgr.getProfileCustomer);

    app.route('/updateaddressdetails/:id') // Update Customer Address Details
        .put(registrationMgr.updateAddressDetails);

    app.route('/updatecarddetails/:id') // Update Card Details
        .put(registrationMgr.updateCardsDetails);

    app.route('/updateprofiledetails/:id') // Update Customer Profile Details
        .put(registrationMgr.updateProfileUpdate);

    app.route('/editcustomeraddress/:id/update/:addressId') // update edited customer address
        .put(registrationMgr.customerAddressUpdate);

    app.route('/editcustomercard/:id/update/:cardId') // update edited customer card details
        .put(registrationMgr.customerCardDetailUpdate);

    app.route('/deletecustomeraddress/:id/delete/:addressId') // Delete single customer address
        .delete(registrationMgr.deleteSeletedAddress);

    app.route('/deletecustomercard/:id/delete/:cardId') // Delete single customer card Details
        .delete(registrationMgr.deleteSeletedCard);

    app.route('/getallcustomer')  // Get All Customer Details  
        .get(registrationMgr.getAllCustomer); 
        
    app.route('/deletecustomer/:id')    // Delete single Customer
        .delete(registrationMgr.deleteCustomer);
    
    app.route('/getcustomeroverallcount')       // customer overall count
        .get(registrationMgr.customerOverallCount);
    
    app.route('/getrecentregisteredcustomer')       // get recent registered customer
        .get(registrationMgr.recentRegisteredCustomer);

    app.route('/gettodayregisteredcustomer')        // get Today registered customer
        .get(registrationMgr.customerTodayCount);

        
    app.route('/createcustomerbyadmin') //  Create Customer By Admin 
        .post(registrationMgr.createCustomerByAdmin);

    app.route('/deletecustomeraccount/:id')         // delete customer account
        .delete(registrationMgr.deleteCustomerAccount);     

    // ERP 
    
    app.route('/loginvendor')               // login vendor
        .post(loginMgr.loginVendor);   
        
        app.route('/createvendor')                  // Registration Vendor Account
        .post(registrationMgr.createVendor);

    app.route('/getallvendor')                  // Get All Vendor
        .get(registrationMgr.getAllVendor);    
    
    app.route('/deleteSingleVendor/:id')                  // Delete Single Vendor
        .delete(registrationMgr.deleteVendor);    


    app.route('/editvendoraddress/:id/update/:addressId') // update edited vendor address
        .put(registrationMgr.vendorAddressUpdate);

    app.route('/deletevendoraddress/:id/delete/:addressId') // Delete single vendor address
        .delete(registrationMgr.deleteSeletedVendorAddress);

    app.route('/uploadvendoraddressdetails/:id') // Upload Vendor Address Details
        .put(registrationMgr.uploadVendorAddressDetails);

    app.route('/getvendorprofile/:id') // Get Vendor Profile
        .get(registrationMgr.getVendorProfile);

    app.route('/editvendorofficeaddress/:id/update/:addressId') // update edited vendor office address
        .put(registrationMgr.vendorOfficeAddressUpdate);

    app.route('/editvendorsupplyaddress/:id/update/:addressId') // update edited vendor supply address
        .put(registrationMgr.vendorSupplyAddressUpdate);

    app.route('/editvendorpayment/:id/update/:addressId') // update edited vendor Payment details
        .put(registrationMgr.vendorPaymentUpdate);

    app.route('/editvendordelivery/:id/update/:addressId') // update edited vendor Delivery details
        .put(registrationMgr.vendorDeliveryUpdate);   
   
    app.route('/editvendorperformance/:id/update/:addressId') // update edited vendor Performance details
        .put(registrationMgr.vendorPerformanceUpdate); 
        
    app.route('/editvendorsignature/:id/update/:addressId') // update edited vendor Signature details
        .put(registrationMgr.vendorSignatureUpdate);     

    app.route('/deletevendorofficeaddress/:id/delete/:addressId') // Delete single vendor office address
        .delete(registrationMgr.deleteSeletedVendorOfficeAddress);

    app.route('/uploadvendorofficeaddressdetails/:id') // Upload Vendor Office Address Details
        .put(registrationMgr.uploadVendorOfficeAddressDetails);    

    app.route('/editvendorwarehouseaddress/:id/update/:addressId') // update edited vendor warehouse address
        .put(registrationMgr.vendorWarehouseAddressUpdate);

    app.route('/deletevendorwarehouseaddress/:id/delete/:addressId') // Delete single vendor warehouse address
        .delete(registrationMgr.deleteSeletedVendorWarehouseAddress);

    app.route('/uploadvendorwarehouseaddressdetails/:id') // Upload Vendor Office warehouse Details
        .put(registrationMgr.uploadVendorWarehouseAddressDetails);  
        
    app.route('/updatevendorcontractdetails/:id')          // update vendor Contract details
        .put(registrationMgr.updateVendorBasicDetails);
    
    app.route('/updatevendorpaymentdetails/:id')        // update vendor Payment Details
        .put(registrationMgr.updateVendorPaymentDetails);

    app.route('/updatevendorotherdetails/:id')      // update vendor Other Details
        .put(registrationMgr.updateVendorOtherDetails);

        app.route('/createvendorbyupload')                  // Registration Vendor Account By Upload XLSX
        .post(registrationMgr.createVendorByUpload);

        app.route('/storecancelchequename/:id')      // add vendor CancelCheque Name
        .put(registrationMgr.storeCancelChequeName);

        app.route('/storedigitalsignaturename/:id')      // add vendor Digital signature Name
        .put(registrationMgr.storeDigitalSignatureName);

        app.route('/updatecancelchequename/:id')      // update vendor Cancel Cheque Name
        .put(registrationMgr.updateImageCancelChequeName);

        app.route('/updatedigitalsignaturename/:id')      // update vendor Digital signature Name
        .put(registrationMgr.updateImageDigitalSignatureName);

    
    // vendor

    app.route('/updatevendorname/:id')              // update vendor name
        .post(registrationMgr.updateName);

    app.route('/updatevendormobile/:id')              // update vendor mobile
        .post(registrationMgr.updateMobile);

    app.route('/updatevendoremail/:id')              // update vendor email
        .post(registrationMgr.updateEmail);

    app.route('/updatepassword/:id')              // Change the password
        .post(registrationMgr.ChangesPassword);

                // subscribe
    app.route('/addsubscribe')
    .post(subscribeMgr.addSubscribe);   
    
app.route('/getallsubscriber')
    .get(subscribeMgr.getSubscribe);  

    app.route('/addsocialmediacustomerforlogin')
        .post(registrationMgr.socialMediaLogin);

        app.route('/getcustomeraddeddate')
        .get(registrationMgr.getCustomerAddedDate);
}
    

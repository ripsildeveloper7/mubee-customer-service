var settingMgr = require('./settingMgr');

module.exports = function (app) {
        // Setting

    app.route('/getallsetting')                 // get all setting
        .get(settingMgr.getAllSetting);        
    
    app.route('/uploadvendorType')              // upload vendor type
        .post(settingMgr.uploadVendorType);    

    app.route('/deletevendorType/:vendorType')         // Detele vendor Type Setting
        .delete(settingMgr.deleteVendorType);   
       
    
    app.route('/uploadcompanystatus')              // upload company Status setting
        .post(settingMgr.uploadCompanyStatus);    

    app.route('/deletecompanystatus/:companyStatus')         // Detele Company status Setting
        .delete(settingMgr.deleteCompanyStatus);   

    
    app.route('/uploadcurrency')              // upload Currency setting
        .post(settingMgr.uploadCurrency);    

    app.route('/deletecurrency/:currency')         // Detele Currency Setting
        .delete(settingMgr.deleteCurrency);       
        
    app.route('/uploadtaxestype')              // upload Taxes type setting
        .post(settingMgr.uploadTaxesType);    

    app.route('/deletetaxestype/:taxesType')         // Detele Taxes type Setting
        .delete(settingMgr.deleteTaxesType);  

    app.route('/uploadmodeofpayment')              // upload Mode of payment setting
        .post(settingMgr.uploadModeOfPayment);    

    app.route('/deletemodeofpayment/:modeOfPayment')         // Detele Mode of payment Setting
        .delete(settingMgr.deleteModeOfPayment);  
        
    app.route('/uploaddeliverymode')              // upload Delivery Mode setting
        .post(settingMgr.uploadDeliveryMode);    

    app.route('/deletedeliverymode/:deliveryMode')         // Detele Delivery Mode Setting
        .delete(settingMgr.deleteDeliveryMode);    
    
    app.route('/uploadreturnsofgoods')              // upload Returns of Good setting
        .post(settingMgr.uploadReturnsOfGoods);    

    app.route('/deletereturnsofgoods/:returnsOfGoods')         // Detele Returns of Good Setting
        .delete(settingMgr.deleteReturnsOfGoods);  
        
    app.route('/uploadmerchandisedivision')              // upload Merchandise Division setting
        .post(settingMgr.uploadMerchandiseDivision);    

    app.route('/deletemerchandisedivision/:merchandiseDivision')         // Detele Merchandise Division Setting
        .delete(settingMgr.deleteMerchandiseDivision);   
        
    app.route('/uploadpaymentday')              // upload Payment setting
        .post(settingMgr.uploadPaymentDays);    

    app.route('/deletepaymentday/:paymentDay')         // Detele Payment Setting
        .delete(settingMgr.deletePaymentDay);     
        
    app.route('/uploadtransportcostby')              // upload Transport Cost Paid By
        .post(settingMgr.uploadTransportCostBy);    

    app.route('/deletetransportcostby/:transportCostPaidBy')         // Detele Transport Cost Paid By
        .delete(settingMgr.deleteTransportCostBy);     

    app.route('/uploadcatalogueimagearrangeby')              // upload Catalogue Image Arranged By
        .post(settingMgr.uploadCatalogueImageArrangeBy);    

    app.route('/deletecatalogueimagearrangeby/:cateImgAssignBy')         // Detele Catalogue Image Arranged By
        .delete(settingMgr.deleteCatalogueImageArrangeBy);         

    app.route('/uploadchargesofphotoshopby')              // upload Charges of photoshop charged By
        .post(settingMgr.uploadChargesOfPhotoShopBy);    

    app.route('/deletechargesofphotoshopby/:chargesOfPhotoShopBy')         // Detele Charges of photoshop charged By
        .delete(settingMgr.deleteChargesOfPhotoShopBy);         

    app.route('/uploadbusinessmodel')              // upload Business Model
        .post(settingMgr.uploadBusinessModel);    

    app.route('/deletebusinessmodel/:businessModel')         // Detele Business Model
        .delete(settingMgr.deleteBusinessModel);             

    app.route('/uploaddefaultdeliverylocation')              // upload Default Delivery Location
        .post(settingMgr.uploadDefaultDeliveryLocation);    

    app.route('/deletedefaultdeliverylocation/:defaultDeliveyLocation')         // Detele Default Delivery Location
        .delete(settingMgr.deleteDefaultDeliveryLocation);         
        
        
    

}
var settingDetails = require('../model/setting.model');

exports.getAllSetting = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    })
}

exports.uploadVendorType = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.vendorType;
                var i = findData[0].vendorType.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].vendorType.push(req.body.vendorType);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteVendorType = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.vendorType;
            var list = findData[0].vendorType;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadCompanyStatus = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.companyStatus;
                var i = findData[0].companyStatus.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].companyStatus.push(req.body.companyStatus);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteCompanyStatus = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.companyStatus;
            var list = findData[0].companyStatus;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadCurrency = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.currency;
                var i = findData[0].currency.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].currency.push(req.body.currency);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteCurrency = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.currency;
            var list = findData[0].currency;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadTaxesType = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.taxesType;
                var i = findData[0].taxesType.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].taxesType.push(req.body.taxesType);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteTaxesType = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.taxesType;
            var list = findData[0].taxesType;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadModeOfPayment = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.modeOfPayment;
                var i = findData[0].modeOfPayment.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].modeOfPayment.push(req.body.modeOfPayment);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteModeOfPayment = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.modeOfPayment;
            var list = findData[0].modeOfPayment;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.uploadDeliveryMode = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.deliveryMode;
                var i = findData[0].deliveryMode.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].deliveryMode.push(req.body.deliveryMode);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteDeliveryMode = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.deliveryMode;
            var list = findData[0].deliveryMode;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.uploadReturnsOfGoods = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.returnsOfGoods;
                var i = findData[0].returnsOfGoods.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].returnsOfGoods.push(req.body.returnsOfGoods);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteReturnsOfGoods = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.returnsOfGoods;
            var list = findData[0].returnsOfGoods;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadMerchandiseDivision = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.merchandiseDivision;
                var i = findData[0].merchandiseDivision.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].merchandiseDivision.push(req.body.merchandiseDivision);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteMerchandiseDivision = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.merchandiseDivision;
            var list = findData[0].merchandiseDivision;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadPaymentDays = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.paymentDay;
                var i = findData[0].paymentDay.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].paymentDay.push(req.body.paymentDay);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deletePaymentDay = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.paymentDay;
            var list = findData[0].paymentDay;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadMerchandiseDivision = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.merchandiseDivision;
                var i = findData[0].merchandiseDivision.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].merchandiseDivision.push(req.body.merchandiseDivision);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteMerchandiseDivision = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.merchandiseDivision;
            var list = findData[0].merchandiseDivision;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadTransportCostBy = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.transportCostPaidBy;
                var i = findData[0].transportCostPaidBy.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].transportCostPaidBy.push(req.body.transportCostPaidBy);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteTransportCostBy = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.transportCostPaidBy;
            var list = findData[0].transportCostPaidBy;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}
exports.uploadCatalogueImageArrangeBy = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.cateImgAssignBy;
                var i = findData[0].cateImgAssignBy.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].cateImgAssignBy.push(req.body.cateImgAssignBy);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteCatalogueImageArrangeBy = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.cateImgAssignBy;
            var list = findData[0].cateImgAssignBy;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadChargesOfPhotoShopBy = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.chargesOfPhotoShopBy;
                var i = findData[0].chargesOfPhotoShopBy.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].chargesOfPhotoShopBy.push(req.body.chargesOfPhotoShopBy);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteChargesOfPhotoShopBy = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.chargesOfPhotoShopBy;
            var list = findData[0].chargesOfPhotoShopBy;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadBusinessModel = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.businessModel;
                var i = findData[0].businessModel.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].businessModel.push(req.body.businessModel);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteBusinessModel = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.businessModel;
            var list = findData[0].businessModel;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

exports.uploadDefaultDeliveryLocation = function(req, res) {
    settingDetails.find({}).select().exec(function(err, findData) {
        if(err) {
            res.status(500).json(err);
        } else {
            if(findData.length === 0) {
                var vendorSetting = new settingDetails(req.body);
                vendorSetting.save(function(err, saveData) {
                    if(err) {
                        res.status(500).json(err);
                    } else {
                        settingDetails.find({}).select().exec(function(err, data) {
                            if(err) {
                                res.status(500).json(err);
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            } else {
                var ID = req.body.defaultDeliveyLocation;
                var i = findData[0].defaultDeliveyLocation.indexOf(ID);
                if (i > -1) {
                    console.log('Exist');
                } else {
                    findData[0].defaultDeliveyLocation.push(req.body.defaultDeliveyLocation);
                    findData[0].save(function (err, data) {
                        if (err) {
                            res.status(500).send({
                                "result": 0
                            });
                        } else {
                            settingDetails.find({}).select().exec(function (err, data) {
                                if (err) {
                                    res.status(500).send({
                                        "result": 'error occured while retreiving data'
                                    })
                                } else {
                                    res.status(200).json(data);
                                }
                            })
                        }
                    })
                }
            }
        }
    })
}

exports.deleteDefaultDeliveryLocation = function (req, res) {
    settingDetails.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).send({
                "result": 'error occured while retreiving data'
            })
        } else {
            var element = req.params.defaultDeliveyLocation;
            var list = findData[0].defaultDeliveyLocation;
            const index = list.indexOf(element);
            if (index !== -1) {
                list.splice(index, 1);
                findData[0].save(function (err, data) {
                    if (err) {
                        res.status(500).send({
                            "result": 'error occured while retreiving data'
                        })
                    } else {
                        settingDetails.find({}).select().exec(function (err, data) {
                            if (err) {
                                res.status(500).send({
                                    "result": 'error occured while retreiving data'
                                })
                            } else {
                                res.status(200).json(data);
                            }
                        })
                    }
                })
            }
        }
    })
}

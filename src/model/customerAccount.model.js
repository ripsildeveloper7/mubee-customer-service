var mongoose = require('mongoose');
var cardDetails = require('./cardDetails.model');
var address = require('./addressDetails.model');
const CustomerDetailsSchema = new mongoose.Schema({
    emailId: String,
    mobileNumber: Number,
    password: String,
    date: Date,
    firstName: String,
    lastName: String,
    dateOfBirth: Date,
    location: String,
    gender: String,
    addressDetails: [address],
    cardDetails: [cardDetails],
});
const Customerdetails = mongoose.model('customerAccount', CustomerDetailsSchema);
module.exports = Customerdetails;
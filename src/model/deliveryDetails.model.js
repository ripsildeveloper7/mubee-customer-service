    var mongoose = require('mongoose');
    const DeliverySchema = new mongoose.Schema({
        slsOT: String,   // service level standards 'On Time' - OT
        slsIFC: String, //  service level standards 'In Full' - case fill
        slsIFL: String,  // service level standards 'In Full' - Line fill
        deliveryMode: String,
        transportCostPaidBy: String,
        returnOfGoods: String,
        minOrderSizeQty: String,  // minimun order size (qty)
        minOrderSizeAmt: String,  // minimun order size (amount)
        cateImgAssignBy: String,  // Catalogue image to be arranged by
        chargesOfPhotoShopBy: String,  // if photoshoot by utsav, charges borne by
        photosCharge: String,
        businessModel:String,
        defaultDeliveyLocation: String
    });
    module.exports = DeliverySchema;
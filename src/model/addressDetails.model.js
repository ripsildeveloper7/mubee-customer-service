    var mongoose = require('mongoose');
    const addressFormSchema = new mongoose.Schema({
        streetAddress: String,
        building: String,
        landmark: String,
        city: String,
        state: String,
        pincode: Number,
        mobileNumber: Number,
        fname: String,
        lname: String,
        country: String
    });
    module.exports = addressFormSchema;
  
var mongoose = require('mongoose');

const ApplyReferAndEarnSchema = new mongoose.Schema({
    couponId: { type: mongoose.Schema.Types.ObjectId, ref: 'referAndEarn' },
    customerId: {type: mongoose.Schema.Types.ObjectId, ref: 'Customerdetails' },
    applyCustomerDetails: [{
        customerId: {type: mongoose.Schema.Types.ObjectId, ref: 'Customerdetails' }, 
        checkApply: {type: Boolean, default: false},
        apply:  {type: Boolean, default: false} 
      }],
      checkApply: {type: Boolean, default: false},
      confirmApply: {type: Boolean, default: false},
      apply:  {type: Boolean, default: false}     
});

const ApplyReferAndEarn = mongoose.model('apply-refer-earn', ApplyReferAndEarnSchema);
module.exports = ApplyReferAndEarn;
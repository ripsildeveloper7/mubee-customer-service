var mongoose = require('mongoose');
const walletSchema = new mongoose.Schema({
userId: String,
loyaltyWallet: Number,
eventWallet: Number,
activityWallet: Number,
referWallet: Number,
activites: [{
    addingCoin: Number,
    withdrawCoin: Number,
    addedDate: Date,
    coinSource: String
}],
creationDate: Date,
expiryDate: Date
});
var wallet = mongoose.model('wallet', walletSchema);
module.exports = wallet;
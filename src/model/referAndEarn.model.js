
var mongoose = require('mongoose');

const ReferAndEarnSchema = new mongoose.Schema({
    couponName: String,
    couponDescription: String,
    referrerPercentage: Number,
    applierPercetage: Number,
    countOfApplier: Number,
    startDate: Date,
    endDate: Date,
    createdDate: { type: Date, default: Date.now()},
    modifiedDate: { type: Date, default: Date.now()},
    confirmApply: Boolean,
    customerCouponDetails: [{
        customerId: {type: mongoose.Schema.Types.ObjectId, ref: 'Customerdetails' }, randomCoupon:  String
      }]
});

const referAndEarn = mongoose.model('referandearn', ReferAndEarnSchema);
module.exports = referAndEarn;
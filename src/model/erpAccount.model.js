var mongoose = require('mongoose');

const ErpLoginSchema = new mongoose.Schema({
    emailId: String,
    password: String
});
const ErpLogin = mongoose.model('erpAccount', ErpLoginSchema);
module.exports = ErpLogin;
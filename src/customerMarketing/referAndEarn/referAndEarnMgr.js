var referAndEarnDA = require('./referAndEarnDA');
var CustomerAccount = require('./../../model/customerAccount.model');
var randomKey = require('random-key-generator'); 
exports.addReferAndEarn = function(req, res) {
    var couponDetails = [];   
    CustomerAccount.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            findData.forEach(el=>
                {
                    var rdmKey =  randomKey.getRandom(10,'REFER','','front').toUpperCase();
                    couponDetails.push({customerId: el._id, randomCoupon: rdmKey});
                })
                try {
                    referAndEarnDA.addReferAndEarn(req, couponDetails, res);
            } catch (error) {
                console.log(error);
            } 
        }
    });
}
exports.getReferAndEarn = function(req, res) {
    try {
        referAndEarnDA.getReferAndEarn(req, res);
} catch (error) {
    console.log(error);
} 
  
}

exports.getSingleCustomerReferAndEarn = function(req, res) {
    try {
        referAndEarnDA.getSingleCustomerReferAndEarn(req, res);
} catch (error) {
    console.log(error);
} 
  
}


exports.getSingleCustomerApplyReferEarn = function(req, res) {
    try {
        referAndEarnDA.getSingleCustomerApplyReferEarn(req, res);
} catch (error) {
    console.log(error);
} 
  
}


exports.getUserReferAndEarnCustomer = function(req, res) {
    try {
        referAndEarnDA.getUserReferAndEarnCustomer(req, res);
} catch (error) {
    console.log(error);
}  
}

exports.updateReferAndEarn = function(req, res) {
    try {
        referAndEarnDA.updateReferAndEarn(req, res);
} catch (error) {
    console.log(error);
}  
}

exports.removeUserReferAndEarnCustomer = function(req, res) {
    try {
        referAndEarnDA.removeUserReferAndEarnCustomer(req, res);
} catch (error) {
    console.log(error);
} 
}


exports.deleteReferAndEarn = function(req, res) {
    try {
        referAndEarnDA.deleteReferAndEarn(req, res);
} catch (error) {
    console.log(error);
} 
}


exports.addUserReferAndEarnApply = function(req, res) {
    try {
        referAndEarnDA.addUserReferAndEarnApply(req, res);
} catch (error) {
    console.log(error);
} 
}
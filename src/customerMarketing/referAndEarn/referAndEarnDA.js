var ReferAndEarn = require('./../../model/referAndEarn.model');
var ApplyReferAndEarn = require('./../../model/applyReferAndEarn.model');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;

exports.addReferAndEarn = function (req, couponDetails, res) {
    var referAndEarn = new ReferAndEarn();
    referAndEarn.couponName = req.body.couponName,
        referAndEarn.couponDescription = req.body.couponDescription,
        referAndEarn.referrerPercentage = req.body.referrerPercentage,
        referAndEarn.applierPercetage = req.body.applierPercetage,
        referAndEarn.countOfApplier = req.body.countOfApplier,
        referAndEarn.startDate = req.body.startDate,
        referAndEarn.endDate = req.body.endDate,
        referAndEarn.customerCouponDetails = couponDetails;
    referAndEarn.save(function (err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            ReferAndEarn.find({}).select().exec(function (err, findData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(findData);
                }
            });
        }
    });
}


exports.getReferAndEarn = function (req, res) {
    ReferAndEarn.find({}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    });
}
exports.getSingleCustomerReferAndEarn = function (req, res) {
    /* ReferAndEarn.find({ "customerCouponDetails.customerId": req.params.customerId }).select('couponName customerCouponDetails referrerPercentage countOfApplier startDate endDate').exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            console.log(findData);
            findData.f
            res.status(200).json(findData);
        }
    }); */
    ReferAndEarn.aggregate([
        { $unwind: "$customerCouponDetails" },
        { $match: { "customerCouponDetails.customerId": ObjectId(req.params.customerId) } }
    ]).exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(findData);
        }
    });
}
/* 

exports.getSingleCustomerApplyReferEarn = function(req, res) {
    var date = new Date();
        date.setHours(0, 0, 0, 0);
    ReferAndEarn.findOne({$and: [ { 
    "customerCouponDetails.applyCustomerDetails.apply": true, 
    "customerCouponDetails.applyCustomerDetails.customerId": req.body.customerId}]}).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if(!findData) {
                ReferAndEarn.findOne({"customerCouponDetails.randomCoupon": req.body.randomCoupon }).select().exec(function (err, findCoupon) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        if(findCoupon && findCoupon.endDate <= date) {
                            ReferAndEarn.findOne({"customerCouponDetails.customerId": req.body.customerId }).select().exec(function (err, findApplyCustomer) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    if(!findApplyCustomer) {
                                        ReferAndEarn.updateMany({ "customerCouponDetails.applyCustomerDetails.customerId": req.body.customerId}, 
                                            {$set:  { "customerCouponDetails.applyCustomerDetails.$[].checkApply": 0 }}
                                           ).select().exec(function (err, findData) {
                                           if (err) {
                                               res.status(500).json(err);
                                           } else {
                                                var pushData = { 
                                                    customerId: req.body.customerId,
                                                    checkApply: true,
                                                    apply: false
                                                };
                                            ReferAndEarn.updateOne({"customerCouponDetails.randomCoupon": req.body.randomCoupon  },
                                            { $push: {"customerCouponDetails.$.applyCustomerDetails": pushData} }).select().exec(function (err, randomData) {
                                               if (err) {
                                                   res.status(500).json(err);
                                               } else {
                                                res.status(200).json(randomData);
                                               }});
                                           }}); 
                                    } else {
                                        res.status(200).json({result: 2});            
                                    }
                                }
                        });
                        } else {
                            ReferAndEarn.findOne({"customerCouponDetails.applyCustomerDetails.customerId": req.body.customerId, 
                            "customerCouponDetails.randomCoupon": req.body.randomCoupon }).select().exec(function (err, findCoupon) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    if(findCoupon.customerCouponDetails.find(e=> e.customerId === req.body.customerId)) {
                                     findCouponCount =  findCoupon.customerCouponDetails.find(e => e.randomCoupon === req.body.randomCoupon).applyCustomerDetails;
                                    if(findCoupon.countOfApplier < findCouponCount.length - 1) {
                                        res.status(200).json(findData);    
                                    } else {
                                        res.status(200).json({result: 0});
                                    }
                                    } else {
                                        res.status(200).json(findData);    
                                    }
                                } 
                        });
                        }
                    }
            });
        } else {
         res.status(200).json({result: 1});
        }
    }});
} */

/* 
exports.getSingleCustomerApplyReferEarn = function (req, res) {
    var date = new Date();
    date.setHours(0, 0, 0, 0);
    // check refer code already used or not
    ReferAndEarn.findOne({
        $and: [{
            "customerCouponDetails.applyCustomerDetails.customerId": req.body.customerId
        }, {
            "customerCouponDetails.applyCustomerDetails.apply": true,
        }]
    }).select().exec(function (err, findData) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (!findData) {
                ReferAndEarn.findOne({ "customerCouponDetails.randomCoupon": req.body.randomCoupon }).select().exec(function (err, couponExpirycheck) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        if (couponExpirycheck && couponExpirycheck.endDate <= date) {
                            ReferAndEarn.findOne({
                                "customerCouponDetails.customerId": req.body.customerId,
                                "customerCouponDetails.randomCoupon": req.body.randomCoupon
                            }).select().exec(function (err, newCustomer) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    if (!newCustomer) {
                                        ReferAndEarn.findOne({
                                            "customerCouponDetails.applyCustomerDetails.customerId": req.body.customerId,
                                        }).select().exec(function (err, randomNumberCustomer) {
                                            if (err) {
                                                res.status(500).json(err);
                                            } else {
                                                if (!randomNumberCustomer) {
                                                    var pushData = {
                                                        customerId: req.body.customerId,
                                                        checkApply: true,
                                                        apply: false
                                                    };
                                                    ReferAndEarn.updateOne({ "customerCouponDetails.randomCoupon": req.body.randomCoupon },
                                                                    { $push: { "customerCouponDetails.$.applyCustomerDetails": pushData } }).select().exec(function (err, randomData) {
                                                                        if (err) {
                                                                            res.status(500).json(err);
                                                                        } else {
                                                                            res.status(200).json({ result: 3 });
                                                                        }
                                                                    });
                                                } else {
                                                    ReferAndEarn.findOneAndUpdate({
                                                        "customerCouponDetails.applyCustomerDetails.customerId":
                                                            req.body.customerId
                                                    },
                                                    { $pull: { "customerCouponDetails.$[].applyCustomerDetails": { "customerId": req.body.customerId } }
                                                        }).select().exec(function (err, randomData) {
                                                            if (err) {
                                                                res.status(500).json(err);
                                                            } else {
                                                                var pushData = {
                                                                    customerId: req.body.customerId,
                                                                    checkApply: true,
                                                                    apply: false
                                                                };
                                                                ReferAndEarn.findOneAndUpdate({
                                                                    "customerCouponDetails.randomCoupon": req.body.randomCoupon
                                                                },
                                                                    { $push: { "customerCouponDetails.$.applyCustomerDetails": pushData } }).select().exec(function (err, alreadyCheckCustomer) {
                                                                        if (err) {
                                                                            res.status(500).json(err);
                                                                        } else {
                                                                            res.status(200).json({ result: 3 });
                                                                        }
                                                                    });
                                                            }
                                                        });

                                                }
                                            }
                                        });
                                    } else {
                                        
                                        ReferAndEarn.findOne({ "customerCouponDetails.randomCoupon": req.body.randomCoupon }).select().exec(function (err, findCoupon) {
                                            if (err) {
                                                res.status(500).json(err);
                                            } else {
                                                
                                                if (findCoupon.customerCouponDetails.find(e => e.customerId == req.body.customerId)) {    
                                                    var findCouponCount = findCoupon.customerCouponDetails.find(e => e.randomCoupon === req.body.randomCoupon).applyCustomerDetails;
                                                    if (findCoupon.countOfApplier < findCouponCount.length - 1) {
                                                        ReferAndEarn.findOne({
                                                            "customerCouponDetails.applyCustomerDetails.customerId": req.body.customerId,
                                                        }).select().exec(function (err, randomNumberCustomer) {
                                                            if (err) {
                                                                res.status(500).json(err);
                                                            } else {
                                                                if (!randomNumberCustomer) {
                                                                    var pushData = {
                                                                        customerId: req.body.customerId,
                                                                        checkApply: true,
                                                                        apply: false
                                                                    };
                                                                    ReferAndEarn.updateOne({ "customerCouponDetails.randomCoupon": req.body.randomCoupon },
                                                                                    { $push: { "customerCouponDetails.$.applyCustomerDetails": pushData } }).select().exec(function (err, randomData) {
                                                                                        if (err) {
                                                                                            res.status(500).json(err);
                                                                                        } else {
                                                                                            res.status(200).json({ result: 4 });
                                                                                        }
                                                                                    });
                                                                } else {
                                                                    ReferAndEarn.findOneAndUpdate({
                                                                        "customerCouponDetails.applyCustomerDetails.customerId":
                                                                            req.body.customerId
                                                                    },
                                                                    { $pull: { "customerCouponDetails.$[].applyCustomerDetails": { "customerId": req.body.customerId } }
                                                                        }).select().exec(function (err, randomData) {
                                                                            if (err) {
                                                                                res.status(500).json(err);
                                                                            } else {
                                                                                var pushData = {
                                                                                    customerId: req.body.customerId,
                                                                                    checkApply: true,
                                                                                    apply: false
                                                                                };
                                                                                ReferAndEarn.findOneAndUpdate({
                                                                                    "customerCouponDetails.randomCoupon": req.body.randomCoupon
                                                                                },
                                                                                    { $push: { "customerCouponDetails.$.applyCustomerDetails": pushData } }).select().exec(function (err, alreadyCheckCustomer) {
                                                                                        if (err) {
                                                                                            res.status(500).json(err);
                                                                                        } else {
                                                                                            res.status(200).json({ result: 4 });
                                                                                        }
                                                                                    });
                                                                            }
                                                                        });
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        res.status(200).json({ result: 2 });  // not eligible to apply
                                                    }
                                                } else {
                                                    res.status(200).json({ result: 1 });   // not exit random code
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        } else {
                            res.status(200).json({ result: 1 }); // coupon not found or expiry
                        }
                    }
                })

            } else {
                res.status(200).json({ result: 0 }); // already applied
            }
        }
    });
} */


exports.getSingleCustomerApplyReferEarn = function (req, res) {
    var date = new Date();
    date.setHours(0, 0, 0, 0);
    // check refer code already used or not
    ReferAndEarn.find({
        $and: [{ "customerCouponDetails.randomCoupon": req.body.randomCoupon },
        { "customerCouponDetails.customerId": req.body.customerId }]
    }).select().exec(function (err, checkCustomer) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (checkCustomer.length > 0) {
                var random = checkCustomer[0].customerCouponDetails.find(el => el.randomCoupon === req.body.randomCoupon);
                if (date <= checkCustomer[0].endDate) {
                    ApplyReferAndEarn.findOne({
                        $and: [{
                            "couponId": random._id
                        }, {"confirmApply": false}]
                    }).select().exec(function (err, couponData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            if (couponData && checkCustomer[0].countOfApplier <= couponData.applyCustomerDetails.length) {
                                ApplyReferAndEarn.updateMany({
                                    $and: [{
                                        "customerId": req.body.customerId
                                    }]
                                }, { $set: { "checkApply": false }}).select().exec(function (err, updateCustomer) {
                                    if (err) {
                                        res.status(500).json(err);
                                    } else {
                                        ApplyReferAndEarn.findOneAndUpdate({
                                            $and: [{
                                                "couponId": random._id
                                            }]
                                        }, { $set: { "checkApply": true }}).select().exec(function (err, updateCustomer) {
                                            if (err) {
                                                res.status(500).json(err);
                                            } else {
                                                res.status(200).json({ result: 3 }); // update successfully
                                            }
                                        })
                                    }
                                })
                            } else {
                                res.status(200).json({ result: 2 }); // not eligible
                            }
                        }
                    
                    
                    
                    });
                } else {
                    res.status(200).json({ result: 0 }); // not found
                }
            } else {
                ReferAndEarn.findOne({ "customerCouponDetails.randomCoupon": req.body.randomCoupon }).select().exec(function (err, couponExpirycheck) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        if (couponExpirycheck && couponExpirycheck.customerCouponDetails.find(e => e.randomCoupon === req.body.randomCoupon) && date <= couponExpirycheck.endDate) {
                            var random = couponExpirycheck.customerCouponDetails.find(e => e.randomCoupon === req.body.randomCoupon)
                            ApplyReferAndEarn.findOne({
                                $and: [{
                                    "applyCustomerDetails.customerId": req.body.customerId
                                }, {
                                    "applyCustomerDetails.apply": true
                                }]
                            }).select().exec(function (err, applyNewCustomer) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    if (!applyNewCustomer) {
                                        ApplyReferAndEarn.findOne({
                                            "couponId": random._id
                                        }).select().exec(function (err, applyCopon) {
                                            if (err) {
                                                res.status(500).json(err);
                                            } else {
                                                if (!applyCopon) {
                                                    var applyCus = [];
                                                    var custData = {
                                                        customerId: req.body.customerId,
                                                        checkApply: true,
                                                        apply: false
                                                    };
                                                    applyCus.push(custData)
                                                    var applyReferAndEarn = new ApplyReferAndEarn();
                                                       applyReferAndEarn.couponId = random._id,
                                                       applyReferAndEarn.customerId = random.customerId,
                                                        applyReferAndEarn.save(function (err, alreadyCheckCustomer) {
                                                            if (err) {
                                                                res.status(500).json(err);
                                                            } else {
                                                                ApplyReferAndEarn.findOneAndUpdate({
                                                                    "applyCustomerDetails.customerId": req.body.customerId
                                                                },
                                                                    {
                                                                        $pull: { "applyCustomerDetails": { "customerId": req.body.customerId } }
                                                                    }).select().exec(function (err, randomData) {
                                                                        if (err) {
                                                                            res.status(500).json(err);
                                                                        } else {
                                                                            ApplyReferAndEarn.findOneAndUpdate({
                                                                                $and: [{
                                                                                    "couponId": random._id,
                                                                                }]
                                                                            }, {
                                                                                $push: {
                                                                                    "applyCustomerDetails": custData
                                                                                }
                                                                            }).select().exec(function (err, updateCustomer) {
                                                                                if (err) {
                                                                                    res.status(500).json(err);
                                                                                } else {
                                                                                    res.status(200).json({ result: 3 });
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                            }
                                                        });
                                                } else {
                                                    var custData = {
                                                        checkApply: true,
                                                        customerId: req.body.customerId,
                                                        apply: false
                                                    }
                                                    ApplyReferAndEarn.findOne({
                                                        $and: [{
                                                            "couponId": random._id
                                                        }, {
                                                            "applyCustomerDetails.customerId": req.body.customerId
                                                        }]
                                                    }).select().exec(function (err, applyCopon) {
                                                        if (err) {
                                                            res.status(500).json(err);
                                                        } else {
                                                            if (!applyCopon) {
                                                                ApplyReferAndEarn.findOneAndUpdate({
                                                                    "applyCustomerDetails.customerId": req.body.customerId
                                                                },
                                                                    {
                                                                        $pull: { "applyCustomerDetails": { "customerId": req.body.customerId } }
                                                                    }).select().exec(function (err, randomData) {
                                                                        if (err) {
                                                                            res.status(500).json(err);
                                                                        } else {
                                                                            ApplyReferAndEarn.findOneAndUpdate({
                                                                                "couponId": random._id
                                                                            }, {
                                                                                $push: {
                                                                                    "applyCustomerDetails": custData
                                                                                }
                                                                            }).select().exec(function (err, newCustomer) {
                                                                                if (err) {
                                                                                    res.status(500).json(err);
                                                                                } else {

                                                                                    res.status(200).json({ result: 3 });
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                            } else {
                                                                ApplyReferAndEarn.findOneAndUpdate({
                                                                    "applyCustomerDetails.customerId": req.body.customerId
                                                                },
                                                                    {
                                                                        $pull: { "applyCustomerDetails": { "customerId": req.body.customerId } }
                                                                    }).select().exec(function (err, randomData) {
                                                                        if (err) {
                                                                            res.status(500).json(err);
                                                                        } else {
                                                                            ApplyReferAndEarn.findOneAndUpdate({
                                                                                "couponId": random._id
                                                                            }, {
                                                                                $push: {
                                                                                    "applyCustomerDetails": custData
                                                                                }
                                                                            }).select().exec(function (err, updateCustomer) {
                                                                                if (err) {
                                                                                    res.status(500).json(err);
                                                                                } else {
                                                                                    res.status(200).json({ result: 3 }); // update succesfully
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    } else {
                                        res.status(200).json({ result: 1 }); // Already Applied          
                                    }
                                }
                                });
                        } else {
                            res.status(200).json({ result: 0 }); // Expiry or not match 
                        }
                    }
                })
            }
        }
    });
}

exports.getUserReferAndEarnCustomer = function (req, res) {
    ApplyReferAndEarn.aggregate([
        { $match: { "customerId": ObjectId(req.params.customerId) } },
        { $match: { "apply": false } },
        { $match: { "checkApply": true } }
    ]).exec(function (err, findCoupon) {
        if (err) {
            res.status(500).json(err);
        } else {
            if (findCoupon[0]) {
                
                ApplyReferAndEarn.aggregate([
                    { $match: { "customerId": ObjectId(req.params.customerId) } },
                    { $match: { "apply": false } },
                    { $match: { "checkApply": true } },
                    {
                        $lookup:
                        {
                            from: "referandearns",
                            localField: "couponId",
                            foreignField: "customerCouponDetails._id",
                            as: "refer"
                        }
                    },
                    { $unwind: "$refer" },
                    { $unwind: "$refer.customerCouponDetails" },
                    { $match: { "refer.customerCouponDetails._id": ObjectId(findCoupon[0].couponId) } }
                ], function (err, couponData) {
                    if (err) {
                        res.status(500).send({
                            message: "no coupon refer"
                        });
                    } else {
                        res.status(200).json(couponData[0]);
                    }
                });
            }
            else {
                ApplyReferAndEarn.aggregate([
                    {$unwind: "$applyCustomerDetails"},
                    { $match: { "applyCustomerDetails.customerId": ObjectId(req.params.customerId) } },
                    { $match: { "apply": false } },
                    { $match: { "applyCustomerDetails.checkApply": true } }
                ]).exec(function (err, checkCoupon) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        if (checkCoupon[0]) {
                            ApplyReferAndEarn.aggregate([
                                {$unwind: "$applyCustomerDetails"},
                                { $match: { "apply": false } },
                                { $match: { "applyCustomerDetails.customerId": ObjectId(req.params.customerId) } },
                                { $match: { "applyCustomerDetails.checkApply": true } },
                                {
                                    $lookup:
                                    {
                                        from: "referandearns",
                                        localField: "couponId",
                                        foreignField: "customerCouponDetails._id",
                                        as: "refer"
                                    }
                                },
                                { $unwind: "$refer" },
                                { $unwind: "$refer.customerCouponDetails" },
                                { $match: { "refer.customerCouponDetails._id": ObjectId(checkCoupon[0].couponId) } }
                            ], function (err, couponData) {
                                if (err) {
                                    res.status(500).send({
                                        message: "no coupon refer"
                                    });
                                } else {
                                    res.status(200).json(couponData[0]);
                                }
                            });
                        }
                        else {
                            res.status(200).json(checkCoupon);
                        }
                    }
                });
            }
        }
    });
}


exports.removeUserReferAndEarnCustomer = function (req, res) {
    ApplyReferAndEarn.findOne({_id: req.params.applyId,  "customerId": req.params.customerId }).select().exec(function (err, customerData) {
            if (err) {
                res.status(500).json(err);
            } else {
                if(customerData){
                    ApplyReferAndEarn.updateMany({
                        $and: [{
                            "customerId": req.params.customerId
                        }]
                    }, { $set: { "checkApply": false }}).select().exec(function (err, updateCustomer) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            ApplyReferAndEarn.aggregate([
                                { $match: { "customerId": ObjectId(req.params.customerId) } },
                                { $match: { "checkApply": true } }
                            ]).exec(function (err, findCoupon) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    if (findCoupon[0]) {
                                        
                                        ApplyReferAndEarn.aggregate([
                                            { $match: { "customerId": ObjectId(req.params.customerId) } },
                                            { $match: { "checkApply": true } },
                                            {
                                                $lookup:
                                                {
                                                    from: "referandearns",
                                                    localField: "couponId",
                                                    foreignField: "customerCouponDetails._id",
                                                    as: "refer"
                                                }
                                            },
                                            { $unwind: "$refer" },
                                            { $unwind: "$refer.customerCouponDetails" },
                                            { $match: { "refer.customerCouponDetails._id": ObjectId(findCoupon[0].couponId) } }
                                        ], function (err, couponData) {
                                            if (err) {
                                                res.status(500).send({
                                                    message: "no coupon refer"
                                                });
                                            } else {
                                                res.status(200).json(couponData[0]);
                                            }
                                        });
                                    } else {
                                        res.status(200).json(findCoupon);
                                    }
                                }
                            });
                        }
                    })
                  
                } else {
                    ApplyReferAndEarn.findOneAndUpdate({ "applyCustomerDetails._id": req.params.id },
                    { $pull: { "applyCustomerDetails": { "_id": req.params.id } } }, { new: true }).select().exec(function (err, randomData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            ApplyReferAndEarn.aggregate([
                                { $unwind: "$applyCustomerDetails" },
                                { $match: { "applyCustomerDetails.customerId": ObjectId(req.params.customerId) } },
                                { $match: { "apply": false } },
                                { $match: { "applyCustomerDetails.checkApply": true } }
                            ]).exec(function (err, findCoupon) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    if (findCoupon[0]) {
                                        ApplyReferAndEarn.aggregate([
                                            { $unwind: "$applyCustomerDetails" },
                                            { $match: { "applyCustomerDetails.customerId": ObjectId(req.params.customerId) } },
                                            { $match: { "apply": false } },
                                            { $match: { "applyCustomerDetails.checkApply": true } },
                                            {
                                                $lookup:
                                                {
                                                    from: "referandearns",
                                                    localField: "couponId",
                                                    foreignField: "customerCouponDetails._id",
                                                    as: "refer"
                                                }
                                            },
                                            { $unwind: "$refer" },
                                            { $unwind: "$refer.customerCouponDetails" },
                                            { $match: { "refer.customerCouponDetails._id": ObjectId(findCoupon[0].couponId) } }
                                        ], function (err, couponData) {
                                            if (err) {
                                                res.status(500).send({
                                                    message: "no coupon refer"
                                                });
                                            } else {
                                                res.status(200).json(couponData[0]);
                                            }
                                        });
                                    }
                                    else {
                                        res.status(200).json(findCoupon);
                                    }
                                }
                            });
                        }
                    });
                }
             }
            });
    
}


exports.addUserReferAndEarnApply = function (req, res) {
    /* ReferAndEarn.findOneAndUpdate({ "customerCouponDetails.applyCustomerDetails._id": req.params.id },
        { $pull: { "customerCouponDetails.$[].applyCustomerDetails": { "_id": req.params.id } }}, {new: true}).select().exec(function (err, randomData) {
            if (err) {
                res.status(500).json(err);
            } else {
                ReferAndEarn.aggregate([
                    { $unwind: "$customerCouponDetails" },
                    { $unwind: "$customerCouponDetails.applyCustomerDetails" },
                    { $match: { "customerCouponDetails.applyCustomerDetails.customerId": ObjectId(req.params.customerId) } },
                    { $match: { "customerCouponDetails.applyCustomerDetails.checkApply": true } }
                ]).exec(function (err, findCoupon) {
                    if (err) {
                        res.status(500).json(err);
                    } else {
                        if(findCoupon[0]){
                            res.status(200).json(findCoupon[0]);
                        } else {
                            res.status(200).json(findCoupon);
                        }
                    }
                });
            }
        }); */
        ApplyReferAndEarn.findOne({ "customerId": req.params.customerId }, function (err, randomData) {
            if (err) {
                res.status(500).json(err);
            } else {
                if(randomData){
                    ApplyReferAndEarn.updateMany({ customerId: req.params.customerId },
                    { $set: { "apply": false } }).select().exec(function (err, randomData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            ApplyReferAndEarn.updateOne({ "customerId": req.params.customerId },
                            { $set: { "apply": true } }).select().exec(function (err, randomData) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    ApplyReferAndEarn.aggregate([
                                        { $match: { "customerId": ObjectId(req.params.customerId) } },
                                        { $match: { "apply": true } }
                                    ]).exec(function (err, findCoupon) {
                                        if (err) {
                                            res.status(500).json(err);
                                        } else {
                                            if (findCoupon[0]) {
                                                
                                                ApplyReferAndEarn.aggregate([
                                                    { $match: { "customerId": ObjectId(req.params.customerId) } },
                                                    { $match: { "apply": true } },
                                                    {
                                                        $lookup:
                                                        {
                                                            from: "referandearns",
                                                            localField: "couponId",
                                                            foreignField: "customerCouponDetails._id",
                                                            as: "refer"
                                                        }
                                                    },
                                                    { $unwind: "$refer" },
                                                    { $unwind: "$refer.customerCouponDetails" },
                                                    { $match: { "refer.customerCouponDetails._id": ObjectId(findCoupon[0].couponId) } }
                                                ], function (err, couponData) {
                                                    if (err) {
                                                        res.status(500).send({
                                                            message: "no coupon refer"
                                                        });
                                                    } else {
                                                        res.status(200).json(couponData[0]);
                                                    }
                                                });
                                            }
                                        }
                                    }); 
                                }
                            });
                        }
                    });
                } else {
                    ApplyReferAndEarn.updateOne({ "applyCustomerDetails.customerId": req.params.customerId },
                    { $set: { "applyCustomerDetails.$[].apply": true } }).select().exec(function (err, randomData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            ApplyReferAndEarn.aggregate([
                                {$unwind: "$applyCustomerDetails"},
                                { $match: { "applyCustomerDetails.customerId": ObjectId(req.params.customerId) } },
                                { $match: { "applyCustomerDetails.apply": true } }
                            ]).exec(function (err, checkCoupon) {
                                if (err) {
                                    res.status(500).json(err);
                                } else {
                                    if (checkCoupon[0]) {
                                        ApplyReferAndEarn.aggregate([
                                            {$unwind: "$applyCustomerDetails"},
                                            { $match: { "applyCustomerDetails.customerId": ObjectId(req.params.customerId) } },
                                            { $match: { "applyCustomerDetails.apply": true } },
                                            {
                                                $lookup:
                                                {
                                                    from: "referandearns",
                                                    localField: "couponId",
                                                    foreignField: "customerCouponDetails._id",
                                                    as: "refer"
                                                }
                                            },
                                            { $unwind: "$refer" },
                                            { $unwind: "$refer.customerCouponDetails" },
                                            { $match: { "refer.customerCouponDetails._id": ObjectId(checkCoupon[0].couponId) } }
                                        ], function (err, couponData) {
                                            if (err) {
                                                res.status(500).send({
                                                    message: "no coupon refer"
                                                });
                                            } else {
                                                res.status(200).json(couponData[0]);
                                            }
                                        });
                                    }
                                    else {
                                        res.status(200).json(checkCoupon);
                                    }
                                }
                            });
                        }
                    });
                }
                
            }
        });
    
}

exports.updateReferAndEarn = function (req, res) {

    ReferAndEarn.findOne({ _id: req.params.id }, function (err, referAndEarn) {
        if (err) {
            res.status(500).json(err);
        } else {
            referAndEarn.couponName = req.body.couponName,
                referAndEarn.couponDescription = req.body.couponDescription,
                referAndEarn.referrerPercentage = req.body.referrerPercentage,
                referAndEarn.applierPercetage = req.body.applierPercetage,
                referAndEarn.countOfApplier = req.body.countOfApplier,
                referAndEarn.startDate = req.body.startDate,
                referAndEarn.endDate = req.body.endDate
            referAndEarn.save(function (err, randomData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    ReferAndEarn.find({}).select().exec(function (err, findData) {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).json(findData);
                        }
                    });
                }
            });
        }
    });
}

exports.deleteReferAndEarn = function (req, res) {

    ReferAndEarn.findOneAndRemove({ "_id": req.params.id }).select().exec(function (err, referAndEarn) {
        if (err) {
            res.status(500).json(err);
        } else {
            ReferAndEarn.find({}).select().exec(function (err, findData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(findData);
                }
            });
        }
    });
}
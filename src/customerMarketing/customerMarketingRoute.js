var referAndEarnMgr = require('./referAndEarn/referAndEarnMgr');
var walletMgr = require('./wallet/walletMgr');

module.exports = function(app) {
    app.route('/addreferandearn')
    .post(referAndEarnMgr.addReferAndEarn);
    app.route('/getreferandearn')
    .get(referAndEarnMgr.getReferAndEarn);
    app.route('/getreferandsinglecustomer/:customerId')
    .get(referAndEarnMgr.getSingleCustomerReferAndEarn);
    app.route('/getreferandsingleapply')
    .put(referAndEarnMgr.getSingleCustomerApplyReferEarn);
    app.route('/getuserreferandearn/:customerId')
    .get(referAndEarnMgr.getUserReferAndEarnCustomer);
    app.route('/removeuserreferandearn/:id/:customerId/:applyId')
    .delete(referAndEarnMgr.removeUserReferAndEarnCustomer);
    app.route('/userreferandearnapply/:customerId')
    .put(referAndEarnMgr.addUserReferAndEarnApply);
    app.route('/updatereferandearn/:id')
    .put(referAndEarnMgr.updateReferAndEarn);
    app.route('/deleterefer/:id')
    .delete(referAndEarnMgr.deleteReferAndEarn);
    // Wallet

    app.route('/createcustomerwallet')
    .post(walletMgr.createCustomerWallet);

    app.route('/getcustomerwallet/:id')
    .get(walletMgr.getCustomerWallet);

    app.route('/addcoininwallet/:id')
    .put(walletMgr.addCoinInWallet);

    app.route('/reducecoininwallet/:id')
    .put(walletMgr.reduceCoinInWallet);

    app.route('/getallcustomerswallet')
    .get(walletMgr.getAllWallet);

    
}
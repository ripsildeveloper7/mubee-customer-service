var WalletModel = require('../../model/wallet.model');

exports.createCustomerWallet = function(req, res) {
    var currentDate = new Date();
    var create = new WalletModel(req.body);
    create.creationDate = currentDate;
    create.expiryDate = currentDate.setMonth(req.body.vaildationPeriodForCoin);
    create.save(function(err, saveData) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(saveData);
        }
    })
}

exports.getCustomerWallet = function(req, res) {
    WalletModel.findOne({userId: req.params.id}).select().exec(function(err, wallet) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(wallet);
        }
    })
}
exports.addCoinInWallet = function(req, res) {
    WalletModel.findOne({userId: req.params.id}).select().exec(function(err, wallet) {
        if (err) {
            res.status(500).json(err);
        } else {
            const obj = {};
            const ar = []; 
            var currentDate = Date.now();
            if (wallet.expiryDate >= currentDate) {
                wallet[req.body.walletType] === undefined ? wallet[req.body.walletType] = req.body.coin : wallet[req.body.walletType] += req.body.coin;
                
                obj.addingCoin = req.body.coin;
                obj.addedDate = currentDate;
                obj.coinSource = req.body.coinActivityDetail;
                ar.push(obj);
                let activity = wallet.activites;
                let array = activity.concat(ar);
                wallet.activites = array;
            } else {
                wallet.loyaltyWallet = 0;
                wallet.eventWallet = 0;
                wallet.activityWallet = 0;
                wallet.referWallet = 0;
                wallet[req.body.walletType] += req.body.coin;
                obj.addingCoin = req.body.coin;
                obj.addedDate = currentDate;
                obj.coinSource = req.body.coinActivityDetail;
                ar.push(obj);
                let activity = wallet.activites;
                let array = activity.concat(ar) 
                wallet.activites = array;
                wallet.expiryDate = currentDate.setMonth(req.body.vaildationPeriodForCoin);
            }
            wallet.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })   
        }
    })
}
exports.reduceCoinInWallet = function(req, res) {
    WalletModel.findOne({userId: req.params.id}).select().exec(function(err, wallet) {
        if (err) {
            res.status(500).json(err);
        } else {
            const obj = {};
            const ar = []; 
            let coin;
            var currentDate = Date.now();
            const loyaltyCoin = req.body.loyaltyWallet;
            const activityCoin = req.body.activityWallet;
            if (loyaltyCoin !== 0) {
                wallet.loyaltyWallet -= loyaltyCoin;
            }
            if (activityCoin !== 0) {
                if (wallet.activityWallet !== 0 && wallet.activityWallet !== undefined) {
                  if (wallet.activityWallet > activityCoin) {
                      wallet.activityWallet -= activityCoin;
                  } else {
                      coin = activityCoin - wallet.activityWallet;
                      wallet.activityWallet = 0;
                      if (coin !== 0) {
                          if (wallet.eventWallet !== 0 && wallet.eventWallet !== undefined) {
                              if (wallet.eventWallet > coin) {
                                  wallet.eventWallet -= eventWallet;
                              } else {
                                  coin = coin - wallet.eventWallet;
                                  wallet.eventWallet = 0;
                                  if (coin !== 0) {
                                      if (wallet.referWallet !== 0 && wallet.referWallet !== undefined) {
                                          if (wallet.referWallet > coin) {
                                              wallet.referWallet -= coin;
                                          } else {
                                              coin = coin - wallet.referWallet;
                                              wallet.referWallet = 0;
                                          }
                                      }
                                  }
                              }
                          } else {
                            if (wallet.referWallet !== 0 && wallet.referWallet !== undefined) {
                                if (wallet.referWallet > coin) {
                                    wallet.referWallet -= coin;
                                } else {
                                    coin = coin - wallet.referWallet;
                                    wallet.referWallet = 0;
                                }
                            }
                          }
                      }
                  }
                  } else {
                    if (wallet.eventWallet !== 0 && wallet.eventWallet !== undefined) {
                        if (wallet.eventWallet > coin) {
                            wallet.eventWallet -= eventWallet;
                        } else {
                            coin = coin - wallet.eventWallet;
                            wallet.eventWallet = 0;
                            if (coin !== 0) {
                                if (wallet.referWallet !== 0 && wallet.referWallet !== undefined) {
                                    if (wallet.referWallet > coin) {
                                        wallet.referWallet -= coin;
                                    } else {
                                        coin = coin - wallet.referWallet;
                                        wallet.referWallet = 0;
                                    }
                                }
                            }
                        }
                    } else {
                      if (wallet.referWallet !== 0 && wallet.referWallet !== undefined) {
                          if (wallet.referWallet > coin) {
                              wallet.referWallet -= coin;
                          } else {
                              coin = coin - wallet.referWallet;
                              wallet.referWallet = 0;
                          }
                      }
                    }
                  }
                }
            obj.withdrawCoin = req.body.loyaltyWallet + req.body.activityWallet;
            obj.addedDate = currentDate;
            obj.coinSource = req.body.coinActivityDetail;
            ar.push(obj);
            let activity = wallet.activites;
            let array = activity.concat(ar);
            wallet.activites = array;
            wallet.save(function(err, updateData) {
                if (err) {
                    res.status(500).json(err);
                } else {
                    res.status(200).json(updateData);
                }
            })
        }
    })
}
exports.getAllWallet = function(req, res) {
    WalletModel.find({}).select().exec(function(err, wallet) {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).json(wallet);
        }
    })
}
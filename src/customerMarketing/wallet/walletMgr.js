var WalletDA = require('./walletDA');

exports.createCustomerWallet = function(req, res) {
    try {
        WalletDA.createCustomerWallet(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getCustomerWallet = function(req, res) {
    try {
        WalletDA.getCustomerWallet(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.addCoinInWallet = function(req, res) {
    try {
        WalletDA.addCoinInWallet(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.reduceCoinInWallet = function(req, res) {
    try {
        WalletDA.reduceCoinInWallet(req, res);
    } catch (error) {
        console.log(error);
    }
}

exports.getAllWallet = function(req, res) {
    try {
        WalletDA.getAllWallet(req, res);
    } catch (error) {
        console.log(error);
    }
}